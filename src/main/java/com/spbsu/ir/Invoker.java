package com.spbsu.ir;

import com.spbsu.ir.net.Net;
import org.apache.commons.math3.linear.ArrayRealVector;

/**
 * spam-filt
 * ksenon, 11:33 06.04.2014
 */
public class Invoker {

  public static void main(final String[] args) {
    Net net = Examples.OR();
    System.out.println(net.forward(new ArrayRealVector(new double[]{0, 1})));
  }

}
