package com.spbsu.ir;

import com.spbsu.ir.func.impl.Sigmoid;
import com.spbsu.ir.net.Net;
import com.spbsu.ir.net.impl.PlainNet;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

/**
 * spam-filt
 * ksenon, 14:15 06.04.2014
 */
public class Examples {

  public static Net NOT() {
    final Net net =  new PlainNet(
            1,
            1,
            1,
            1.,
            new double[]{1, 1},
            new int[]{1},
            new Sigmoid(),
            new RealMatrix[]{
                    new Array2DRowRealMatrix(new double[][]{
                            {-30, 20}
                    }),
                    new Array2DRowRealMatrix(new double[][]{
                            {1, 0}
                    })
            });
    net.createNet();
    return net;
  }

  public static Net AND() {
    final Net net = new PlainNet(
            1,
            2,
            1,
            1,
            new double[]{1, 1},
            new int[]{1},
            new Sigmoid(),
            new RealMatrix[]{
                    new Array2DRowRealMatrix(new double[][]{
                            {20, 20, -30}
                    }),
                    new Array2DRowRealMatrix(new double[][]{
                            {1, 0}
                    })
            }
    );
    net.createNet();
    return net;
  }

  public static Net OR() {
    final Net net = new PlainNet(
            1,
            2,
            1,
            1,
            new double[]{1, 1},
            new int[]{1},
            new Sigmoid(),
            new RealMatrix[]{
                    new Array2DRowRealMatrix(new double[][]{
                            {20, 20, -10}
                    }),
                    new Array2DRowRealMatrix(new double[][]{
                            {1, 0}
                    })
            }
    );
    net.createNet();
    return net;
  }

}
