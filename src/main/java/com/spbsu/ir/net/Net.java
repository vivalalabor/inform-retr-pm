package com.spbsu.ir.net;

import com.spbsu.ir.func.Rectifier;
import com.spbsu.ir.layer.Layer;
import com.sun.istack.internal.NotNull;
import org.apache.commons.math3.linear.RealVector;

/**
 * spam-filt
 * ksenon, 13:23 06.04.2014
 */
public abstract class Net {

  protected final Layer[] layers;
  protected final Rectifier rectifier;
  protected final int inputDimension;
  protected final int outputDimension;
  protected final double globalBias;
  protected final double[] layersBias;

  protected Net(final int layerNumber,
                final int inputDimension,
                final int outputDimension,
                final double globalBias,
                final double[] layersBias,
                final @NotNull Rectifier rectifier
  ) {
    layers = new Layer[layerNumber];
    this.inputDimension = inputDimension;
    this.outputDimension = outputDimension;
    this.globalBias = globalBias;
    this.layersBias = layersBias;
    this.rectifier = rectifier;
  }

  public Layer[] getLayers() {
    return layers;
  }

  public abstract void createNet();

  public abstract RealVector forward(final @NotNull RealVector x);

}
