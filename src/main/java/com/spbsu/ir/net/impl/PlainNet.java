package com.spbsu.ir.net.impl;

import com.spbsu.ir.func.Rectifier;
import com.spbsu.ir.layer.impl.PlainLayer;
import com.spbsu.ir.net.Net;
import com.sun.istack.internal.NotNull;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

/**
 * spam-filt
 * ksenon, 13:30 06.04.2014
 */
public class PlainNet extends Net {

  private final int layerNumber;
  private final int[] nodeNumber;
  private RealMatrix[] weights;

  protected PlainNet(
          final int layerNumber,
          final int inputDimension,
          final int outputDimension,
          final int[] nodeNumber,
          final double globalBias,
          final double[] layersBias,
          final @NotNull Rectifier rectifier
  ) {
    super(layerNumber, inputDimension, outputDimension, globalBias, layersBias, rectifier);
    this.layerNumber = layerNumber;
    this.nodeNumber = nodeNumber;
    weights = new RealMatrix[layerNumber + 1];
  }

  public PlainNet(
          final int layerNumber,
          final int inputDimension,
          final int outputDimension,
          final double globalBias,
          final double[] layersBias,
          final int[] nodeNumber,
          final @NotNull Rectifier rectifier,
          final @NotNull RealMatrix[] weights
  ) {
    this(
            layerNumber,
            inputDimension,
            outputDimension,
            nodeNumber,
            globalBias,
            layersBias,
            rectifier
    );
    this.weights = weights;
  }

  @Override
  public void createNet() {
    layers[0] = new PlainLayer(nodeNumber[0] + 1, layersBias[0], rectifier);
    //weights[0] = new Array2DRowRealMatrix(nodeNumber[0], inputDimension + 1);

    for (int i = 1; i < layerNumber; i++) {
      layers[i] = new PlainLayer(nodeNumber[i] + 1, layersBias[i], rectifier);
      //weights[i] = new Array2DRowRealMatrix(nodeNumber[i + 1], nodeNumber[i] + 1);
    }

    //weights[layerNumber + 1] = new Array2DRowRealMatrix(outputDimension, nodeNumber[layerNumber - 1] + 1);
  }

  @Override
  public RealVector forward(@NotNull RealVector x) {
    RealVector input = new ArrayRealVector(x.toArray(), new double[]{globalBias});
    RealVector result = layers[0].activate(weights[0].transpose().preMultiply(input));

    for (int i = 1; i < layerNumber; i++) {
      result = layers[i].activate(weights[i].transpose().preMultiply(result));
    }

    return weights[layerNumber].transpose().preMultiply(result);
  }

}
