package com.spbsu.ir.layer;

import com.spbsu.ir.func.Rectifier;
import com.sun.istack.internal.NotNull;
import org.apache.commons.math3.linear.RealVector;

/**
 * spam-filt
 * ksenon, 13:09 06.04.2014
 */
public abstract class Layer {

  protected final int nodeNumber;
  protected final double bias;
  protected final Rectifier rectifier;

  protected Layer(final int nodeNumber, final double bias, final @NotNull Rectifier rectifier) {
    this.nodeNumber = nodeNumber;
    this.bias = bias;
    this.rectifier = rectifier;
  }

  public int getNodeNumber() {
    return nodeNumber;
  }

  public double getBias() {
    return bias;
  }

  public Rectifier getRectifier() {
    return rectifier;
  }

  public abstract RealVector activate(final @NotNull RealVector a);

}
