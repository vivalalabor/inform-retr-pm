package com.spbsu.ir.layer.impl;

import com.spbsu.ir.func.Rectifier;
import com.spbsu.ir.layer.Layer;
import com.sun.istack.internal.NotNull;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

/**
 * spam-filt
 * ksenon, 13:18 06.04.2014
 */
public class PlainLayer extends Layer {

  public PlainLayer(int nodeNumber, double bias, @NotNull Rectifier rectifier) {
    super(nodeNumber, bias, rectifier);
  }

  @Override
  public RealVector activate(@NotNull RealVector a) {
    RealVector result = new ArrayRealVector(a.getDimension() + 1);
    for (int i = 0; i < a.getDimension(); i++) {
      result.setEntry(i, rectifier.f(a.getEntry(i)));
    }
    result.setEntry(a.getDimension(), bias);
    return result;
  }

}
