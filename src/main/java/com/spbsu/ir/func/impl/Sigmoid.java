package com.spbsu.ir.func.impl;


import com.spbsu.ir.func.Rectifier;

/**
 * spam-filt
 * ksenon, 12:50 06.04.2014
 */
public class Sigmoid implements Rectifier {

  @Override
  public double f(double x) {
    return 1 / (1 + Math.exp(-x));
  }

}
