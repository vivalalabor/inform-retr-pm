package com.spbsu.ir.func;

/**
 * spam-filt
 * ksenon, 12:47 06.04.2014
 */
public interface Rectifier {

  double f(final double x);

}
