package com.spbsu.ir.layer;

import com.spbsu.ir.func.impl.Sigmoid;
import com.spbsu.ir.layer.impl.PlainLayer;
import org.apache.commons.math3.linear.*;
import org.junit.Assert;
import org.junit.Test;

/**
 * spam-filt
 * ksenon, 13:15 06.04.2014
 */
public class LayerTest extends Assert {

  @Test
  public void activate() {
    Layer layer = new PlainLayer(5, 1, new Sigmoid());
    RealVector a = new ArrayRealVector(new double[]{-10, -5, 0, 5, 10});

    System.out.println(layer.activate(a));
  }

  @Test
  public void mult() {
    RealMatrix A = new Array2DRowRealMatrix(new double[][]{
            {1, 2, 5, 6},
            {3, 4, 7, 8}
    });
    RealVector x = new ArrayRealVector(new double[]{
            1, 1, 1, 1
    });

    System.out.println(A.transpose().preMultiply(x));
  }

}
